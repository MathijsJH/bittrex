<?php

namespace MathijsJH\Bittrex;

use MathijsJH\Bittrex\Exceptions\ApiException;
use MathijsJH\Bittrex\Exceptions\ConnectionException;
use MathijsJH\Bittrex\Exceptions\InvalidMarketException;

class Bittrex
{

    const BASEURI = 'https://bittrex.com/api/v1.1/';

    const APIGROUP_PUBLIC = 'public';
    const APIGROUP_MARKET = 'market';
    const APIGROUP_ACCOUNT = 'account';

    protected $apikey;
    protected $secret;

    /**
     * Bittrex constructor.
     *
     * An API key / secret can be created @ https://bittrex.com/Manage#sectionApi
     *
     * @param string $apikey
     * @param string $secret
     */
    public function __construct(string $apikey, string $secret)
    {
        $this->apikey = trim($apikey);
        $this->secret = trim($secret);
    }

    /**
     * Get ticker data for a given market for the current moment
     *
     * @param string $market
     *
     * @throws InvalidMarketException
     * @throws ApiException
     * @throws ConnectionException
     *
     * @return \stdClass
     */
    public function ticker(string $market): \stdClass
    {

        $this->validateMarket($market);

        $response = $this->execute(static::APIGROUP_PUBLIC, 'getticker', ['market' => $market]);

        return $response->result;

    }

    /**
     * Validates if a string is following the correct market markup
     *
     * A valid market contains two pairs of three letters, seperated by a dash.
     *
     * @param string $market
     *
     * @throws InvalidMarketException
     */
    public function validateMarket(string $market): void
    {

        if ( ! preg_match('/^[A-Z]{3}-[A-Z]{3}$/', $market)) {
            throw new InvalidMarketException($market);
        }

    }

    /**
     * Execute API call
     *
     * @param string $apiGroup
     * @param string $action
     * @param array $params
     *
     * @return stdClass
     * @throws ApiException
     * @throws ConnectionException
     */
    protected function execute(string $apiGroup, string $action, array $params = []): \stdClass
    {

        if (static::APIGROUP_PUBLIC !== $apiGroup) {
            $params['apikey'] = $this->apikey;
            $params['nonce']  = time();
        }

        $url = static::BASEURI . $apiGroup . '/' . $action . '?' . http_build_query($params);
        $ch  = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (static::APIGROUP_PUBLIC !== $apiGroup) {
            $sign = hash_hmac('sha512', $url, $this->secret);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('apisign:' . $sign));
        }

        $result = curl_exec($ch);

        if (false === $result) {
            throw new ConnectionException();
        }

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (200 !== $statusCode) {
            throw new ConnectionException(sprintf('Couldn\'t complete action, statuscode: %d', $statusCode));
        }

        $json = json_decode($result);

        if (false === $json->success) {
            throw new ApiException($json->message);
        }

        return $json;
    }

    /**
     * Get market information
     *
     * @throws ApiException
     * @throws ConnectionException
     *
     * @return array
     */
    public function markets(): array
    {

        $response = $this->execute(static::APIGROUP_PUBLIC, 'getmarkets');

        return $response->result;

    }

    /**
     * Create a buy order with a limit
     *
     * @param string $market
     * @param float $amount
     * @param float $rate
     *
     * @throws InvalidMarketException
     * @throws ApiException
     * @throws ConnectionException
     *
     * @return mixed
     */
    public function buyLimit(string $market, float $amount, float $rate)
    {

        $this->validateMarket($market);

        $response = $this->execute(static::APIGROUP_MARKET, 'buylimit',
            [
                'market'   => $market,
                'quantity' => number_format($amount, 8),
                'rate'     => number_format($rate, 8)
            ]
        );

        return $response->result;

    }

    /**
     * Create a sell order with a limit
     *
     * @param string $market
     * @param float $amount
     * @param float $rate
     *
     * @throws InvalidMarketException
     * @throws ApiException
     * @throws ConnectionException
     *
     * @return mixed
     */
    public function sellLimit(string $market, float $amount, float $rate)
    {

        $this->validateMarket($market);

        $response = $this->execute(static::APIGROUP_MARKET, 'selllimit',
            [
                'market'   => $market,
                'quantity' => number_format($amount, 8),
                'rate'     => number_format($rate, 8)
            ]
        );

        return $response->result;

    }

    /**
     * Get open orders
     *
     * @param string $market
     *
     * @throws InvalidMarketException
     * @throws ApiException
     * @throws ConnectionException
     *
     * @return mixed
     */
    public function getOpenOrders(string $market)
    {

        $this->validateMarket($market);

        $response = $this->execute(static::APIGROUP_MARKET, 'getopenorders');

        return $response->result;
    }

    /**
     * Get order by order id
     *
     * @param string $orderId
     *
     * @return mixed
     * @throws ApiException
     * @throws ConnectionException
     */
    public function order(string $orderId)
    {
        $response = $this->execute(static::APIGROUP_ACCOUNT, 'getorder', [
            'uuid' => $orderId,
        ]);

        return $response->result;
    }

    /**
     * Cancel an order
     *
     * @param string $orderId
     *
     * @return bool
     * @throws ApiException
     * @throws ConnectionException
     */
    public function cancelOrder(string $orderId): bool
    {

        $response = $this->execute(static::APIGROUP_MARKET, 'cancel', [
            'uuid' => $orderId,
        ]);

        return $response->success;
    }

    /**
     * @return mixed
     * 
     * @throws ApiException
     * @throws ConnectionException
     */
    public function balances() 
    {
        $response = $this->execute(static::APIGROUP_ACCOUNT, 'getbalances');

        return $response->result;
    }

    /**
     * @return mixed
     * 
     * @throws ApiException
     * @throws ConnectionException
     */
    public function marketSummaries()
    {
        $response = $this->execute(static::APIGROUP_PUBLIC, 'getmarketsummaries');

        return $response->result;
    }
}
