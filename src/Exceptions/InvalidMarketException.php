<?php
namespace MathijsJH\Bittrex\Exceptions;

use Throwable;

class InvalidMarketException extends \Exception {

	public function __construct( string $market ) {

		$message = sprintf('Market excpects a string of the format XXX-XXX, instead \'%s\' was provided.', $market);

		parent::__construct( $message, 0, null );

	}

}